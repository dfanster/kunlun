# FFmpeg中Video子系统示例程序使用指南 this is a typo. fix it.

昆仑芯的视频和图片编解码基于FFmpeg，提供了两种使用方式：代码中调用的方式及FFmpeg命令行的方式：

- 代码中调用的方式，我们针对常用硬件加速功能提供了Demo源码参考。

- 产出提供的命令行调用可执行程序都是使用的动态链接的方式，所以在使用命令行程序执行前需先配置动态链接库的路径，方式如下：

  
    ```bash
    export LD_LIBRARY_PATH=\[lib_path]:$LD_LIBRARY_PATH
    ```


FFmpeg命令行程序依赖 ffmpeg库 , xppi库, xre库, 所以需要配置库的路径到LD_LIBRARY_PATH环境变量中
以XECV软件包 ubuntu2004 系统 x86_64 架构 XECV安装路径为 /home/work/output/xecv-ubuntu2004_x86_64 为例:

- 如果使用系统的XRE库，则只需要配置ffmpeg和xppi库对应的路径，配置如下   
```bash
   XECV_DIR=/home/work/output/xecv-ubuntu2004_x86_64  
   export LD_LIBRARY_PATH=$XECV_DIR/xppi/lib:$XECV_DIR/ffmpeg/lib::$LD_LIBRARY_PATH  
```
- 如果系统里没有XRE库，则需要配置ffmpeg库，xppi库和XRE库对应的路径, 以x86_64架构 5.0.10.1版本的XRE为例，配置如下  
```bash
   XECV_DIR=/home/work/output/xecv-ubuntu2004_x86_64  
   cd $XECV_DIR  
   tar -zxvf xre-Linux-x86_64-5.0.10.1.tar.gz   
   export LD_LIBRARY_PATH=$XECV_DIR/xre-Linux-x86_64-5.0.10.1/so:$XECV_DIR/xppi/lib:$XECV_DIR/ffmpeg/lib::$LD_LIBRARY_PATH  
```

## FFmpeg中XECV Demo使用说明

### Video JPEG

- Video JPEG

   ​Video JPEG 解码示例程序，支持单进程多线程解码；昆仑芯R200系列产品可支持H.264、HEVC、JPEG和MJPEG的解码。
   
   ```bash
   ./xecv_decode
   Usage: ./xecv_decode <input_file> <thread_number> <stream_loop> <yuv_scale_width> <yuv_scale_height> <output_file>
   input_file: input file name
   thread_number: 1 ... 200
   stream_loop: 0, 1.  just support mjpeg, not support one frame picture
   yuv_scale_width: width of  output yuv file
   yuv_scale_height: height of output yuv file
   output_file: output file name
   if Video source is usb camera, 'input_file' must be '/dev/video0'
   ./xecv_decode ~/1920x1080.jpg 1 0 1280 720 yuv_outfile
   ./xecv_decode ~/1920x1080.h264 1 0 1280 720 yuv_outfile
   ```
   
   | **参数**         | **说明**                                                     | **选项** |
   | ---------------- | ------------------------------------------------------------ | -------- |
   | input file       | 指定待解码Video JPEG文件名，支持单张JPEG文件和MJPEG文件; 待解码的视频码流，可支持mp4、flv等常见封装格式、裸流、rtsp流。 | 必选     |
   | thread_num       | 解码线程数，线程数上限是200。                                | 必选     |
   | cyc_read_file    | 解码结束后，是否循环解码，默认不循环解码。<br />0：不循环解码；1：循环解码 | 必选     |
   | yuv_scale_width  | Video JPEG解码输出yuv数据的宽。                              | 必选     |
   | yuv_scale_height | Video JPEG解码输出yuv数据的高。                              | 必选     |
   | output_file_yuv  | 输出yuv数据文件名。                                          | 可选     |
   [命令参数详解]





- Demo源码在产出目录中的路径：

  ​XECV/xecv-ubuntu_x86_64/ffmpeg/share/ffmpeg/examples/xecv_decode.c

- 示例：

    执行如下命令：
    
    ```bash
    ./xecv_decode ~/1920x1080.jpg 1 0 1280 720 yuv_outfile
    ```
    
    生成如下文件：
    
    yuv_outfile_0_jpeg_1280x720_stride_1280x720.yuv
    
    操作截图如下：
    
    ![img](demo.png)

### Video JPEG编码Demo

- Video JPEG硬件编码: klx_encode

   ​Video JPEG硬件编码示例程序，支持多线程编码。输入yuv的数据，输出JPEG图片或者视频文件，编码的同时可统计瞬时（统计时长1s）和平均的编码性能。
   
   ```bash
   Usage: ./klx_encode <codec> <input file> <thread_sum> <cyc_read_file> <width> <height>
   <sw_format> <frame_rate> <bit_rate> <is_h2d> [output file]
   ```
   
   <>表示必选项;[]表示可选项
   
   示例如下：
   
   ```bash
   ./klx_encode mjpeg_klxenc ~/Big_Buck_Bunny_1920x1080_30fps_1frames_nv12.yuv 1 0 1920 1080 nv12 1 0 1 outfile
   ```
   
   | **参数**      | **说明**                                                     | **选型** |
   | ------------- | ------------------------------------------------------------ | -------- |
   | codec         | 编码器：支持 mjpeg_klxenc、 h264_klxenc和hevc_klxenc。       | 必选     |
   | input file    | yuv文件的路径。                                              | 必选     |
   | thread_sum    | 编码线程的个数，一个线程为一路编码，值为1\~128。              | 必选     |
   | cyc_read_file | 是否对Nv12的输入文件进行循环编码：<br />1：循环编码；0：不循环编码。 | 必选     |
   | width         | yuv文件的宽。                                                | 必选     |
   | height        | yuv文件的高。                                                | 必选     |
   | sw_format     | 输入源文件yuv的格式。                                        | 必选     |
   | frame_rate    | 设定的编码帧率，范围1\~60。                                   | 必选     |
   | bit_rate      | 码率，范围0, 10000\~95000000；0代表内部自适应码率。           | 必选     |
   | is_h2d        | 是否每帧从host拷贝yuv数据到device：<br />1：是 ； 0：否      | 必选     |
   | \[output file] | 编码输出文件的路径。                                         | 可选     |
   [命令参数详解]
- Demo源码在产出目录中的路径：

   ​ XECV/xecv-ubuntu_x86_64/ffmpeg/share/ffmpeg/examples/klx_encode.c

- 示例：

    执行如下命令：
    
    ```bash
    ./klx_encode mjpeg_klxenc ~/Big_Buck_Bunny_1920x1080_30fps_1frames_nv12.yuv 1 0 1920 1080 nv12 1 0 1 out.jpg
    ```
    
    生成文件：out_t0_1920x1080_nv12_2073600bps.jpg
    
    操作截图如下：
    
    ![img](../images/xecv_demo_0005.png)

### 视频图像间转码

- Video到Video/Jpeg硬件转码: klx_transcode

    Video硬件解码得到yuv数据之后，可以直接使用Video/Jpeg硬件编码器进行Video/Jpeg图片编码。
    
    ​Video transcode是硬件解码之后硬件编码的示例程序，解码之后的yuv数据不需要同步到cpu内存，编码器直接从ddr中读取待编码的yuv数据。
    
    
    ```bash
    ./klx_transcode
    Usage: ./klx_transcode <codec> <input file> <output file>
    ```
    
    <>表示必选项；[]表示可选项。
    
    示例：
    
    ```bash
    ./klx_transcode h264_klxenc ~/Big_Buck_Bunny_1080_30fps_300frames_30MB.mp4 output.mp4
    ```
    
    | **参数**    | **说明**                                                     | **选型** |
    | ----------- | ------------------------------------------------------------ | -------- |
    | codec       | 编码器：支持 mjpeg_klxenc, h264_klxenc, hevc_klxenc。        | 必选     |
    | input file  | 仅支持H.264、HEVC编码格式的视频文件，解码器根据文件自动识别。 | 必选     |
    | output file | 编码输出文件。                                               | 必选     |
    [命令参数详解]
- Demo源码在FFmpeg 产出目录中的路径：

 XECV/xecv- ubuntu_x86_64/ffmpeg/share/ffmpeg/examples/klx_transcode.c

- 示例：

    执行如下命令：
    
    ```bash
    ./klx_transcode h264_klxenc ~/Big_Buck_Bunny_1080_30fps_300frames_30MB.mp4 output.mp4
    ```
    
    生成文件：output.mp4
    
    操作截图如下：
    
    ![img](../images/xecv_demo_0007.png)

# FFmpeg命令行参数和使用说明

在某些业务场景中，会直接使用FFmpeg命令行的方式，进行XECV的业务部署，昆仑芯XECV软件栈支持这种使用方式，FFmpeg命令行使用Demo如下所述。

## FFmpeg命令行：视频解码

### 视频解码

- 命令行：

```bash
./ffmpeg -hwaccel xecv -c:v h264_xevid -i ~/media_resource/for_decoder/jellyfish-3-mbps-hd-h264.h264 -y -vsync 0  h264_xevid.yuv
```

| **参数名**      | **参数说明**                                                 |
| -------------------- | ------------------------------------------------------------ |
| -hwaccel             | xecv：昆仑芯的Video Codec硬件加速器                         |
| -c:v                 | h264_xevid：H.264解码器<br />hevc_xevid：HEVC解码器 |
| -i                   | 指定待解码的文件名。                                          |
| h264_xevid.yuv | 解码输出的yuv文件。                                           |
[参数说明]
### 视频解码和图像缩放

- 命令行：

```bash
./ffmpeg -hwaccel xecv -resize 640x480 -c:v h264_xevid -i ~/media_resource/for_decoder/jellyfish-3-mbps-hd-h264.h264 -y -vsync 0  h264_xrx.yuv
```



| **参数名** | **参数类型** | **取值范围**            | **默认值**                 | **参数说明**                  |
| ---------- | ------------ | ----------------------- | -------------------------- | ----------------------------- |
| -resize | String       | Video解码器分辨率范围内 | NULL：和码流分辨率保持一致 | 指定解码器输出的yuv数据分辨率 |
[参数说明]
### 视频解码和图像crop

- 命令行：

```bash
./ffmpeg -hwaccel xecv -crop 100x200x300x400 -c:v h264_xevid -i ~/media_resource/for_decoder/jellyfish-3-mbps-hd-h264.h264 -y -vsync 0  h264_fxx.yuv
```


| **参数名**     | **参数类型** | **取值范围** | **默认值** | **参数说明**                                                 |
| -------------- | ------------ | ------------ | ---------- | ------------------------------------------------------------ |
| -crop | String       |              | NULL       | -crop:Video decoder输出yuv进行crop，"(top)x(bottom)x(left)x(right) 100x200x300x400 "<br />各参数含义：<br />(top)表示上边距：100<br />(bottom)表示下边距：200<br />(left)表示左边距：300<br />(right)表示右边距：400 |
[参数说明]

### 视频解码和抽帧

大部分AI推理的业务场景，并不需要将视频码流中的每一帧都送给AI模型进行推理，解码后要进行抽帧处理。在AI推理整个业务流程中，

-vf fps=10,如果源视频的fps=30，现在是每解码三帧取一帧数据。

- 命令行

```bash
./ffmpeg -hwaccel xecv -c:v h264_xevid -i jellyfish-3-mbps-hd-h264.mkv -vf fps=10 -vsync 0  drop.yuv
```


| **参数名** | **参数类型** | **取值范围** | **默认值** | **参数说明**                                                 |
| ---------- | ------------ | ------------ | ---------- | ------------------------------------------------------------ |
| -vf fps=10 | int          | 大于0,小于源视频fps         | 源视频fps          | -vf fps=10,如果源视频的fps=30,现在是每解码3帧取一帧数据 |
[参数说明]
- 示例：

    命令：
    
    ```bash
    ./ffmpeg -hwaccel xecv -c:v h264_xevid -i jellyfish-3-mbps-hd-h264.mkv -vf fps=10 -vsync 0  drop.yuv
    ```

生成文件：drop.yuv

    ![img](../images/xecv_demo_0009.png)

## FFmpeg 命令行：JPEG解码


```bash
./ffmpeg -hwaccel xecv -c:v mjpeg_xevid -i \~/mjpg_1920x1080_ver264_yuv420p.mjpg mjpg_1920x1080.yuv

```


| **参数名**      | **参数说明**                                                 |
| --------------- | ------------------------------------------------------------ |
| -hwaccel        | xecv:指定昆仑芯的JPEG解码加速器                            |
| -c:v            | 指定解码器名字                                               |
[参数说明]
## FFmpeg命令行：视频编码

- 命令行

```bash
./ffmpeg -hwaccel xecv -s 1920x1080 -pix_fmt nv12 -i ~/media_resource/for_encoder/CrowdRun_1920x1080_nv12_40.yuv -vf hwupload_xecv -vcodec h264_klxenc -y h264_klxenc.h264
```

| **参数**         | **类型** | **取值范围**           | **默认值**                      | **参数说明**                                                 |
| ---------------- | -------- | ---------------------- | ------------------------------- | ------------------------------------------------------------ |
| -hwaccel         | string   | xecv                 | xecv                          | xecv：昆仑芯的Video Codec硬件加速器                        |
| -s | string | - | - | 输入源文件YUV的宽x高 |
| -pix_fmt         | string   | - | - | 编码器支持的yuv数据格式：yuv420p, nv12, yuyv422, p010le |
| -b:v             | int      | 10000- levelMax        | 1000000                         | 10000..levelMax, target bitrate for rate control, in bps     |
| -bf              | int      | 0-6                    | 0                               | set maximum number of B-frames between non-B-frames，        |
| -g               | int      | 0-                     | 250                             | Forces every Nth frame to be encoded as intra frame. 0 = Do not force |
| -r               | int      | 1- 1048575             | 30                              | Frame rate                                                   |
| -profile         | string   | - | HEVC:main, H.264:high          | HEVC: main, main10   <br />H.264:baseline, main, high        |
| -level           | string   | - | HEVC: 6.0, H.264: 5.1           | HEVC: 1.0, 2.0, 2.1, 3.0, 3.1, 4.0, 4.1, 5.0, 5.1, 5.2, 6.0 <br />H.264: 1.0, 1.1, 1.2, 1.3, 2.0, 2.1, 2.2, 3.0, 3.1, 3.2, 4.0, 4.1, 4.2, 5.0, 5.1    |
| -tier            | string   | - | main                            | main, high                                                   |
| -qp              | int      | -1 - 51                | -1                              | -1(disable) Constant quantization parameter rate control method |
| -cq              | int      | 0 - 51                 | 0                               | 0(disable) Constant rate factor mode, working with rc-lookahead turned on |
| -rc-lookahead    | int      | 0, 4 - 20              | 0                               | The number of look-ahead frames <br />0 = Disable 2-pass encoding. <br />4-20 = Set the number of look-ahead frames and enable 2-pass encoding. |
| -vf        | - | - | - | hwupload_xecv:昆仑芯Video Codec实现图像数据从host 端ddr同步到device端gddr显存的滤镜。 |
| -vcodec    | - | - | - | 指定昆仑芯视频编码器 H.264: h264_klxenc  HEVC: hevc_klxenc。   |
[参数说明]
## FFmpeg命令行：JPEG编码


```bash
./ffmpeg -hwaccel xecv -s 1920x1080 -i \~/Big_Buck_Bunny_1920x1080_30fps_1frames_nv12.yuv -vf hwupload_xecv -b 23000000 -r 30 -vcodec mjpeg_klxenc %03d.jpg
```


| **参数名** | **参数类型** | **取值范围**   | **默认值** | **参数说明**                                                 |
| ---------- | ------------ | -------------- | ---------- | ------------------------------------------------------------ |
| -hwaccel         | string   | xecv                 | xecv                          | xecv：昆仑芯的Video Codec硬件加速器                        |
| -b         | int          | 10000\~95000000 | 2000000    | 指定编码码率。                                               |
| -r         | int          | 1\~60           | 30         | 指定帧率。                                                   |
| -vf        | - | - | - | hwupload_xecv:昆仑芯JPEG Codec实现图像数据从host 端ddr同步到device端gddr显存的滤镜。 |
| -vcodec    | - | - | - | mjpeg_klxenc:指定昆仑芯JPEG编码器。                       |
[参数说明]
## FFmpeg命令行硬件转码

昆仑芯的Video Codec可以实现H.264和HEVC之间的高性能转码功能，解码后的yuv数据驻留在ddr，直接作为视频编码的输入。转码的同时，也可以结合已实现的编解码参数实现不同的功能，如解码参数-resize，在解码时进行yuv数据缩放，进而编码成指定分辨率的码流。

### 命令行：H.264解码，HEVC编码


```bash
./ffmpeg -hwaccel xecv -c:v h264_xevid -hwaccel_output_format xecv -i \~/media_resource/for_decoder/jellyfish-3-mbps-hd-h264.h264 -c:v hevc_klxenc -c:a copy -vsync 0 -y h264_transcode_hevc.hevc
```

| -hwaccel                 | xecv:昆仑芯的Video Codec硬件加速器。                         |
| ------------------------ | ------------------------------------------------------------ |
| -c:v                     | -i之前为配置解码器：<br />h264_xevid：H.264解码器 <br />hevc_xevid：HEVC解码器 ，<br />-i之后为配置编码器： <br />mjpeg_klxenc: JPEG编码器 <br />h264_klxenc: H.264编码器 <br />hevc_klxenc：HEVC编码器 |
| -hwaccel_output_format   | xecv ,指定解码输出格式为xecv格式，此格式数据在显存中，编码器直接进行编码 |
| -i                       | 指定待解码的文件名。                                         |
| h264_transcode_hevc.hevc | 转码输出码流文件，支持裸流 .jpg .h264 .hevc文件， 也支持 .mp4 .flv等封装格式。 |
[参数说明]
### 命令行：H.264解码，H.264编码


```bash
./ffmpeg -hwaccel xecv -c:v h264_xevid -hwaccel_output_format xecv -i \~/media_resource/for_decoder/jellyfish-3-mbps-hd-h264.h264 -c:v h264_klxenc -c:a copy -vsync 0 -y h264_transcode_h264.h264

```

### 命令行：HEVC解码，H.264编码


```bash
./ffmpeg -hwaccel xecv -c:v hevc_xevid -hwaccel_output_format xecv -i \~/media_resource/for_decoder/CrowdRun_1920x1080_ver265_5000000.hevc -c:v h264_klxenc -c:a copy -vsync 0 -y hevc_transcode_h264.h264
```


### 命令行：HEVC解码，HEVC编码


```bash
./ffmpeg -hwaccel xecv -c:v hevc_xevid -hwaccel_output_format xecv -i \~/media_resource/for_decoder/CrowdRun_1920x1080_ver265_5000000.hevc -c:v hevc_klxenc -c:a copy -vsync 0 -y hevc_transcode_hevc.hevc

```

### 命令行：H.264解码，JPEG编码


```bash
./ffmpeg -hwaccel xecv -c:v h264_xevid -hwaccel_output_format xecv -i \~/myhome_1080p.flv  -c:v mjpeg_klxenc  -b:v 0  -r 5 %03d.jpg
```


### 命令行：HEVC解码，JPEG编码


```bash
./ffmpeg -hwaccel xecv -c:v hevc_xevid -hwaccel_output_format xecv  -i \~/Jellyfish_hevc1080_10s_30MB.mp4  -c:v mjpeg_klxenc -b:v 0  -r 5 %03d.jpg

```

